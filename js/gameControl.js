// Variables

let lifes = 3;
let score = 0;
var meshes = [];


//Functions

function getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
}

function SpawnBall() {
    let dataMeshes = [];
    var mesh = new THREE.Mesh(geometry, material);
    mesh.position.x = (Math.random() - 0.5) * 10;
    mesh.position.y = (Math.random() - 0.5) * 10;
    mesh.position.z = -50;
    dataMeshes.push(mesh);
    dataMeshes.push(getRandomArbitrary(-0.02, 0.02));
    dataMeshes.push(getRandomArbitrary(-0.02, 0.02));
    dataMeshes.push(score / 40 + 0.01);
    meshes.push(dataMeshes);
    scene.add(mesh);
}

function addScore(points) {
    score += points;
    document.getElementById("scoreSpan").innerHTML = score;
    document.getElementById("scoreSpan2").innerHTML = score;
}

function removeLife() {
    document.getElementById("life" + lifes).style.display = "none";
    lifes -= 1;
}

function openGameOverDiv() {
    document.getElementById("GameOverScreen").style.display = "block";
    document.getElementById("GameOverDiv").style.width = "100vw";
}

function onMouseClick(e) {
    e.preventDefault();

    mouse.x = (e.clientX / window.innerWidth) * 2 - 1;
    mouse.y = -(e.clientY / window.innerHeight) * 2 + 1;

    raycaster.setFromCamera(mouse, camera);

    var intersects = raycaster.intersectObjects(scene.children, true);
    for (var i = 0; i < intersects.length; i++) {
        addScore(1);
        SpawnBall();
        // GSAP
        this.tl = new TimelineMax();
        this.tl.to(intersects[i].object.position, 2, {
            z: "-=400"
        });
        this.tl.to(intersects[i].object, 0, {
            visible: false,
            ease: Expo.easeOut
        });
    }
}

// Init Scene
var scene = new THREE.Scene();

//Init Camera
var camera = new THREE.PerspectiveCamera(
    75,
    window.innerWidth / window.innerHeight,
    0.1,
    1000
);
camera.position.z = 4;

// Renderer
var renderer = new THREE.WebGLRenderer({
    antialias: true
});
renderer.setClearColor(0x000000);
renderer.setSize(window.innerWidth, window.innerHeight);

document.body.appendChild(renderer.domElement);

// Responsive
window.addEventListener("resize", () => {
    renderer.setSize(window.innerWidth, window.innerHeight);
    camera.aspect = window.innerWidth / window.innerHeight;

    camera.updateProjectionMatrix();
});

// Detect Click
var raycaster = new THREE.Raycaster();
var mouse = new THREE.Vector2();

// Balls creation
var geometry = new THREE.SphereGeometry(1, 10, 6);
var material = new THREE.MeshLambertMaterial({
    color: "white",
    wireframe: true
});

// First balls spawn
for (let i = 0; i < 2; i++) {
    var dataMeshes = [];
    var mesh = new THREE.Mesh(geometry, material);
    mesh.position.x = (Math.random() - 0.5) * 10;
    mesh.position.y = (Math.random() - 0.5) * 10;
    mesh.position.z = -50;
    dataMeshes.push(mesh);
    dataMeshes.push(getRandomArbitrary(-0.02, 0.02));
    dataMeshes.push(getRandomArbitrary(-0.02, 0.02));
    dataMeshes.push(0.04);

    meshes.push(dataMeshes);
    scene.add(mesh);
}

// Light Init
var light = new THREE.PointLight(0xffffff, 2, 500);
light.position.set(0, 0, 25);
scene.add(light);

var render = () => {
    // Bals movement
    for (var i = 0; i < meshes.length; i++) {
        meshes[i][0].rotation.x += 0.01;
        meshes[i][0].rotation.y += 0.01;
        meshes[i][0].rotation.z += 0.01;
        meshes[i][0].position.x += meshes[i][1];
        meshes[i][0].position.y += meshes[i][2];
        meshes[i][0].position.z += meshes[i][3];
        if (meshes[i][0].position.z >= 0) {
            removeLife();
            meshes[i][0].position.z = -5000;
            SpawnBall();
        }
    }

    // Detect GO
    if (lifes === 0) {
        openGameOverDiv();
    }

    requestAnimationFrame(render);
    renderer.render(scene, camera);
};

render();

window.addEventListener("click", onMouseClick);

function Restart() {
    document.location.reload(true);
}